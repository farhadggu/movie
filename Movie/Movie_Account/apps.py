from django.apps import AppConfig


class MovieAccountConfig(AppConfig):
    name = 'Movie_Account'
