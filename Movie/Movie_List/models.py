from django.db import models
from django.utils import timezone
from embed_video.fields import EmbedVideoField



class Movie(models.Model):
    title = models.CharField(max_length=200, verbose_name="Movie Name")
    slug = models.SlugField(max_length=200, verbose_name="Name in URL")
    imdb = models.CharField(max_length=10, verbose_name="IMDB")
    genre = models.TextField(verbose_name="Genres")
    publish = models.DateField(default=timezone.now, verbose_name="Publish Date")
    desc = models.TextField(verbose_name="About Movie")
    trailer = EmbedVideoField(verbose_name="Trailer", null=True, blank=True)
    download = models.URLField(verbose_name="480P")
    download2 = models.URLField(verbose_name="720P")
    download3 = models.URLField(verbose_name="1080P")
    img = models.ImageField(upload_to="media", verbose_name="Image")
    img2 = models.ImageField(upload_to="media", verbose_name="Detail Page Image (Big)")
    season = models.CharField(max_length=20, blank=True, null=True, verbose_name="Season?")
    time = models.CharField(max_length=100, verbose_name="Movie Time")
    director = models.CharField(max_length=100, verbose_name="Movie Director")
    stars = models.TextField(verbose_name="Stars")

    def __str__(self):
        return self.title


class Series(models.Model):
    title = models.CharField(max_length=200, verbose_name="Series Name")
    slug = models.SlugField(max_length=200, verbose_name="Name in URL")
    imdb = models.CharField(max_length=10, verbose_name="IMDB")
    genre = models.TextField(verbose_name="Genres")
    publish = models.DateField(default=timezone.now, verbose_name="Publish Date")
    desc = models.TextField(verbose_name="About Series")
    trailer = EmbedVideoField(verbose_name="Trailer", null=True, blank=True)
    img = models.ImageField(upload_to="media", verbose_name="Image")
    img2 = models.ImageField(upload_to="media", verbose_name="Detail Page Image (Big)")
    season = models.CharField(max_length=20, blank=True, null=True, verbose_name="Season?")
    director = models.CharField(max_length=100, verbose_name="Series Director")
    stars = models.TextField(verbose_name="Series Stars")
    status = models.BooleanField(default=True, verbose_name="Series Mode")

    def __str__(self):
        return self.title


class SeriesEpisode(models.Model):
    title = models.CharField(max_length=200, verbose_name="Series Name")
    season = models.IntegerField(blank=True, null=True, verbose_name="Season")
    episode = models.IntegerField(blank=True, null=True, verbose_name="Episode")
    series = models.ManyToManyField(Series, related_name="movies", verbose_name="Which Series?")
    download = models.URLField(verbose_name="720P")
    download2 = models.URLField(verbose_name="1080P")
    

    def __str__(self):
        return self.title