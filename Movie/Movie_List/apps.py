from django.apps import AppConfig


class MovieListConfig(AppConfig):
    name = 'Movie_List'
