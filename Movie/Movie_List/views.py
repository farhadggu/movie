from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView
from django.core.paginator import Paginator
from .models import Movie, Series, SeriesEpisode
from django.db.models import Q
from itertools import chain


def home(request):
    movie = Movie.objects.all()[:8]
    series = Series.objects.all()[:12]
    context = {
        'movie': movie,
        'series': series,
    }
    return render(request, 'home_page.html', context)


def detail(request, slug):
    movie = get_object_or_404(Movie, slug=slug)
    context = {
        'detail': movie
    }
    return render(request, 'detail_movie.html', context)


class MovieList(ListView):
    model = Movie
    paginate_by = 10
    template_name = "view_all_movie.html"


#===========================> Series Section
def detail_series(request, slug):
    series = get_object_or_404(Series, slug=slug)
    context = {
        'detail': series
    }
    return render(request, 'detail_serires.html', context)


class SeriesList(ListView):
    model = Series
    paginate_by = 10
    template_name = "view_all_series.html"


#=====================================> Search Section
class SearchList(ListView):
    paginate_by = 10
    template_name = 'search_list.html'

    def get_queryset(self):
        search = self.request.GET.get('q')
        movie = Movie.objects.filter(Q(title__icontains=search) | Q(stars__icontains=search))
        series = Series.objects.filter(Q(title__icontains=search) | Q(stars__icontains=search))
        result = chain(movie, series)
        result_list = list(result)
        #or_lookup = (Q(title__icontains=search) | Q(stars__icontains=search))
        return result_list

    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('q')
        return context
