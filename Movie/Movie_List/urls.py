from django.urls import path
from .views import home, detail, MovieList, detail_series, SeriesList, SearchList

app_name = "movie"
urlpatterns = [
    path('', home, name="home"),
    path('detail-movie/<slug:slug>', detail, name='detail'),
    path('all-movie/page/<int:page>', MovieList.as_view(), name='movie-list'),
    path('detail-series/<slug:slug>', detail_series, name='detail-series'),
    path('all-series/page/<int:page>', SeriesList.as_view(), name='series-list'),
    path('search/', SearchList.as_view(), name="search"),
    path("search/page/<int:page>", SearchList.as_view(), name="search"),

]
