from django.contrib import admin
from embed_video.admin import AdminVideoMixin
from .models import Movie, Series, SeriesEpisode


class MovieAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'title', 'imdb', 'publish']
    prepopulated_fields = {'slug':('title',)}
    ordering = ['publish']


admin.site.register(Movie, MovieAdmin)


class SeriesAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'title', 'imdb', 'publish']
    prepopulated_fields = {'slug':('title',)}
    ordering = ['publish']


admin.site.register(Series, SeriesAdmin)


class SeriesEpisodeAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'title', 'season', 'episode']
    ordering = ['title']

admin.site.register(SeriesEpisode, SeriesEpisodeAdmin)