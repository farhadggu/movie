# Generated by Django 3.1.7 on 2021-03-18 12:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Movie_List', '0007_remove_seriesepisode_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='seriesepisode',
            name='status',
            field=models.BooleanField(default=True, verbose_name='Series Mode'),
        ),
    ]
